
COMPTE RENDU PROJET DEVOPS

Fonctionnalités fournies:

Notre bibliothèque propose une gestion des colonnes et des dataframes avec les possibilitées obligatoires demandées, on peut :

    -Créer un dataframe à partir de données entrées manuellement, ou à partir d'un fichier .csv(avec n'importe quel séparateur)
    -Modifier un dataframe en ne gardant que certaines lignes ou colonnes, ou encore en ne gardant que le résultat d'une requête "WHERE"
    -Afficher un dataframe en entier,à partir du début/de la fin ou afficher seulement le maximum/minimum des valeurs de chaque colonne d'un dataframe



Choix d'outils:

    -GitLab, pour la gestion des versions et l'intégration continue
    -Maven, pour la génération du projet,la construction et lancement des tests automatiques, et la couverture de code (avec le plugin javacoco)
    -JUnit, pour les tests unitaires
    -Docker et DockerHub, pour le déploiement continu
    -Intellij, en tant qu'IDE
    
DockerHub:


Feedback:
    Nous avons eu quelques difficultés à prendre en main certains outils, notamment Maven ou Docker.