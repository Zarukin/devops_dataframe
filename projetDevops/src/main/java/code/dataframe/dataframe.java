package dataframe;
import dataframe.Exception.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class dataframe {
	public List<column> columns;
	public int datasize;
	public String name;

	//crée un dataframe à partir d'une liste de colone d'un nom et d'une taille
	private dataframe(String name,List<column> col,int size){
		datasize=size;
		columns=col;
		this.name=name;
	}

	//crée un dataframe à partir d'un nom et de List la premiere List doit être la List de nom des colonnes
	public dataframe(String name ,List<String> nameList,List<?>... tab) throws Exception{
		int nbcol=0; //permet d'avoir le nombre de colonne
		this.name=name;
		columns=new ArrayList();
		for (List<?> tempList : tab) {
				nbcol++;
				if (nbcol == 1)
					datasize =tempList.size();
				if (nameList.size() < nbcol)//si il y'a plus de colonne que de nom
					throw new MoreColumnThanName("");
				if (datasize != tempList.size() ){// nombre de ligne dans la colonne différent du nombre dans la  dataframe
					throw new NotSameSizeOfDataInColumn("");
				}
				if (tempList.size() == 0) {
					columns.add(new column("inconnu",nameList.get(nbcol-1),tempList));
				}else {
					columns.add(new column(tempList.get(0).getClass().getSimpleName(),nameList.get(nbcol-1),tempList));
				}
			}
	}
	//crée un dataframe à partir d'un .csv avec , comme séparateur par défaut
	public dataframe(String name,String filename) throws Exception{
		if(!filename.substring(filename.length()-4,filename.length()).equals(".csv")) {//vérifie le suffixe.csv
			throw new NotCsvFile("");// si ce n'est pas le bon format
		}
		this.name=name;
		BufferedReader br = new BufferedReader(new FileReader(filename)); //on ouvre le fichier
		String line;
		datasize=-1;
		String []namecol=null;
		int size=0;
		columns = new LinkedList();
		while((line =br.readLine()) != null){
			String[] tabline= line.split(",");
			if (datasize == -1){
				namecol=tabline;
				size=tabline.length;
			}else if (datasize == 0){
				if (size !=tabline.length){
					throw new LineSizeDifferentOfNumberOfColumn("");// pas toutes les ligne on des données pour toute les colonne
				}
				for (String temp:tabline){ // on crée toute les colonnes en déduisan les types
					columns.add(GetColumnOfCSVData(temp,namecol[columns.size()]));
				}
				for (int i =0;i<tabline.length;i++){//on rajoute les donner dans les colonnes
					columns.get(i).AddData(typeConverter(tabline[i]));
				}
			}else {
				if (size !=tabline.length){
					throw new LineSizeDifferentOfNumberOfColumn("");//pas toute les ligne on des données pour toute les colonne
				}
				for (int i =0;i<tabline.length;i++){//on rajoute les donner dans les colonnes
					columns.get(i).AddData(typeConverter(tabline[i]));
				}
			}
			datasize++; // on augmente la taille des données
		}
	}
	//crée un dataframe à partir d'un .csv avec splitcar comme séparateur
	public dataframe(String name,String filename,String splitcar) throws Exception{
		if(!filename.substring(filename.length()-4,filename.length()).equals(".csv")) {//vérifie le suffixe.csv
			throw new NotCsvFile("");// si ce n'est pas le bon format
		}
		this.name=name;
		BufferedReader br = new BufferedReader(new FileReader(filename)); //on ouvre le fichier
		String line;
		datasize=-1;
		String []namecol=null;
		int size=0;
		columns = new LinkedList();
		while((line =br.readLine()) != null){
			String[] tabline= line.split(splitcar);
			if (datasize == -1){
				namecol=tabline;
				size=tabline.length;
			}else if (datasize == 0){
				if (size !=tabline.length){
					throw new LineSizeDifferentOfNumberOfColumn("");// pas tous les ligne on des données pour toute les colonne
				}
				for (String temp:tabline){ // on crée toute les colonnes en déduisan les types
					columns.add(GetColumnOfCSVData(temp,namecol[columns.size()]));
				}
				for (int i =0;i<tabline.length;i++){//on rajoute les donner dans les colonnes
					columns.get(i).AddData(typeConverter(tabline[i]));
				}
			}else {
				if (size !=tabline.length){
					throw new LineSizeDifferentOfNumberOfColumn("");//pas toute les ligne on des données pour toute les colonne
				}
				for (int i =0;i<tabline.length;i++){//on rajoute les donner dans les colonnes
					columns.get(i).AddData(typeConverter(tabline[i]));
				}
			}
			datasize++; // on augmente la taille des données
		}
	}
	//affiche toute la dataframe
	public void Print()throws Exception{
		System.out.println("------------------------"+name+"------------------------");
		System.out.print("indice" + "\t");
		for( column temp:columns) {//on affiche le nom et le type des colonne
			System.out.print(temp.getName()+ "["+temp.getType()+"]"+ "\t");
		}
		System.out.println();
		for (int i=0;i<datasize;i++) {
			System.out.print(i + " :\t");// on affiche les données de chaque colonne
			for( column temp:columns) {
				System.out.print(temp.getDataOfInd(i) +"\t");
			}
			System.out.println();
		}
	}
	//affiche toute les size dernier élément de la dataframe
	public void PrintEnd(int size)throws Exception{
		System.out.println("------------------------"+name+"------------------------");
		System.out.print("indice" + "\t");
		for( column temp:columns) {
			System.out.print(temp.getName()+ "["+temp.getType()+"]"+ "\t");
		}
		System.out.println();
		int begin=datasize-size > 0 ? datasize-size:0;
		for (int i=begin;i<datasize;i++) {
			System.out.print(i + " :\t");
			for( column temp:columns) {
				System.out.print(temp.getDataOfInd(i) +"\t");
			}
			System.out.println();
		}
	}
	//affiche toute les size premier élément de la dataframe
	public void PrintBegin(int size)throws Exception{
		System.out.println("------------------------"+name+"------------------------");
		System.out.print("indice" + "\t");
		for( column temp:columns) {
			System.out.print(temp.getName()+ "["+temp.getType()+"]"+ "\t");
		}
		System.out.println();
		int end=datasize > size ? size:datasize;
		for (int i=0;i<end;i++) {
			System.out.print(i + " :\t");
			for( column temp:columns) {
				System.out.print(temp.getDataOfInd(i) +"\t");
			}
			System.out.println();
		}
	}
	//crée un dataframe seullement avec les colonne étant dans  la liste labels
	public dataframe dataframeFromColumnLabel(String... labels) throws Exception{
		LinkedList<column> col =new LinkedList<column>();
		for (String label: labels){// pour chaque label
			int find =0 ;
			for (column c:columns ){
				if (c.getName().equals(label)){//on regarde si c'est le bon label
					col.add(c.clone()); // on clone la colonne dans les nouvelle colonne du nouveau dataframe
					find++;
					break;
				}
			}
			if(find != 1) // le label n'est pas dans al dataframe
				throw new LabelDoNotExist("");
		}
		return new dataframe(name,col,datasize);
	}
	//crée un dataframe seullement avec les lignes étant dans  la liste inds
	public dataframe dataframeFromLineIndex(int... inds) throws Exception{
		LinkedList<column> col =new LinkedList<column>();
		int tab[] = new int[datasize];

		for (column c: columns){
			List<Object> tempCol = new ArrayList<Object>();
			for (int ind :inds){
				if ( ind < datasize && ind > -1){ // on prend l'indice à la colonne et on l'ajoute à la nouvelle
					tempCol.add(c.getDataOfInd(ind));
				}else{
					throw new InvalidIndices("");// cas indice supérieur à la taille ou inférieur à 0
				}
			}
			col.add(new column(c.getType(),c.getName(),tempCol));
		}
		return new dataframe(name,col,inds.length);
	}

	//crée un dataframe seullement avec les lignes étant dans  la liste inds
	public dataframe dataframeFromLineIndex(List<Integer> inds) throws Exception{
		LinkedList<column> col =new LinkedList<column>();
		int tab[] = new int[datasize];

		for (column c: columns){
			List<Object> tempCol = new ArrayList<Object>();
			for (int ind :inds){
				if ( ind < datasize && ind > -1){
					tempCol.add(c.getDataOfInd(ind));
				}else{
					throw new InvalidIndices("");//cas indice supérieur à la taille ou inférieur à 0
				}
			}
			col.add(new column(c.getType(),c.getName(),tempCol));
		}
		return new dataframe(name,col,inds.size());
	}

	public dataframe dataframeWhere(String label,Object value) throws Exception{
		List<Integer> ind= null;
		for (column c:columns ){
			if (c.getName().equals(label)){
				ind =c.search(value); // on récupére les indice quequels il y a la valeur
				return dataframeFromLineIndex(ind); // on crée un data frame à partir de c'est ligne
			}
		}
		throw new LabelDoNotExist("");// cas ou le label n'est pas une colonne du  dataframe
	}
	//affiche si possible le maximum pour chaque colonne
	public void printMax(){
		System.out.println(name+" maximum colonne :");
		Object[] value;
		for( column temp:columns) {
			System.out.print(temp.getName()+ "["+temp.getType()+"]"+"(Indexe)"+ "\t");
		}
		System.out.println();
		for (column c:columns){
			if (c.getType().equals("Integer")){
				value=c.getMaxValue();
				System.out.print(value[1] +"("+value[0]+")\t");
			}else if (c.getType().equals("Double")){
				value=c.getMaxValue();
				System.out.print(value[1] +"("+value[0]+")\t");
			}else if (c.getType().equals("Float")){
				value=c.getMaxValue();
				System.out.print(value[1] +"("+value[0]+")\t");
			}else{
				System.out.print("Donnée non comparable\t");
			}
		}
		System.out.println();
	}
	//affiche si possible le minimum pour chaque colonne
	public void printMin(){
		System.out.println(name+" minimum colonne :");
		Object[] value;
		for( column temp:columns) {
			System.out.print(temp.getName()+ "["+temp.getType()+"]"+"(Indexe)"+ "\t");
		}
		System.out.println();
		for (column c:columns){
			if (c.getType().equals("Integer")){
				value=c.getMinValue();
				System.out.print(value[1] +"("+value[0]+")\t");
			}else if (c.getType().equals("Double")){
				value=c.getMinValue();
				System.out.print(value[1] +"("+value[0]+")\t");
			}else if (c.getType().equals("Float")){
				value=c.getMinValue();
				System.out.print(value[1] +"("+value[0]+")\t");
			}else{
				System.out.print("Donnée non comparable\t");
			}
		}
		System.out.println();
	}
	//affiche si possible la moyenne pour chaque colonne
	public void printMoy(){
		System.out.println(name+" moyenne colonne :");
		Object[] value;
		for( column temp:columns) {
			System.out.print(temp.getName()+ "["+temp.getType()+"]"+ "\t");
		}
		System.out.println();
		for (column c:columns){
			if (c.getType().equals("Integer")){
				System.out.print(c.getMoyValue());
			}else if (c.getType().equals("Double")){
				System.out.print(c.getMoyValue());
			}else if (c.getType().equals("Float")){
				System.out.print(c.getMoyValue());
			}else{
				System.out.print("Donnée non comparable\t");
			}
		}
		System.out.println();
	}

	/*public dataframe dataframeGroupby(){

	}*/
	//crée une colonne à partir d'un nom et d'un String val en déduisant le type de val
	private column GetColumnOfCSVData(String val,String name){
		int number=0;
		for (int i = 0;i<val.length();i++){
			if(val.charAt(i) > 47 && val.charAt(i) <58){
				number++;
			}
		}
		if (number==val.length()){
			return new column("Integer",name,new  LinkedList<Integer>());
		}else if (number==val.length()-1 && val.contains(".")){
			return new column("Double",name,new  LinkedList<Double>());
		}else {
			return new column("String",name,new  LinkedList<String>());
		}
	}
	//crée un objet en déduisant le type de val (convertit val dans le bon type)
	private Object typeConverter(String val){
		int number=0;
		for (int i = 0;i<val.length();i++){
			if(val.charAt(i) > 47 && val.charAt(i) <58){
				number++;
			}
		}
		if (number==val.length()){
			return Integer.parseInt(val);
		}else if (number==val.length()-1 && val.contains(".")){
			return Double.parseDouble(val);
		}else {
			return val;
		}
	}

	public boolean equals( Object anObject ){

		if( anObject instanceof dataframe ) {
			dataframe frame = (dataframe) anObject;
			return this.datasize==frame.datasize && this.columns.equals(frame.columns) && this.name.equals(frame.name);
		}
		return false;
	}

	@Override
	public String toString() {
		String s = "";
		s+=("------------------------"+name+"------------------------");
		s+=("indice" + "\t");
		for( column temp:columns) {//on affiche le nom et le type des colonne
			s+=(temp.getName()+ "["+temp.getType()+"]"+ "\t");
		}
		s+=("\n");
		for (int i=0;i<datasize;i++) {
			s+=(i + " :\t");// on affiche les données de chaque colonne
			for( column temp:columns) {
				try {
					s+=(temp.getDataOfInd(i) +"\t");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			s+=("\n");
		}
		return s;
	}

}
