package dataframe;
import dataframe.Exception.InvalidIndices;

import java.io.InvalidObjectException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class column {
	public String type;
	public String name;
	List<Object> data;

	public column(String type,String name,List<?> d){
		this.type=type;
		this.name=name;
		data=(List<Object>)d;
	}
	//renvoie le nom stocker dans la colonne
	public String getName() {
		return name;
	}
	//renvoie le type stocké dans la colone
	public String getType() {
		return type;
	}
	public List<Object> getData() {
		return data;
	}
	//renvoie l'objet à l'indice ind pour cette colonne
	public Object getDataOfInd(int ind) throws Exception{
		if (ind < data.size() && ind >-1)
			return data.get(ind);
		else 
			throw new InvalidIndices(""); // indice invalide
	}
	//ajoute l'objet t à la fin de cette colonne
	public void AddData(Object t) throws Exception{
		if (t.getClass().getSimpleName().toString().equals(type)){
			data.add(t);
		}else {
			throw new InvalidObjectException(""); // type de l'objet invalide
		}
	}
	//suprime l'objet à l'indice ind de la colonne
	public void removeData(int ind ) throws Exception{
		if (ind < data.size() && ind >-1)
			data.remove(ind);
		else
			throw new InvalidIndices(""); //indice invalide
	}
	//copie de la liste de donnée
	public column clone(){
		List<Object> colData=new ArrayList<Object>();
		for(Object j:data){
			colData.add(j);
		}
		return new column(type,name,colData);
	}
	//cherche l'object value dans la colonne
	public List<Integer> search(Object value){
		List<Integer> ind=new LinkedList<Integer>();
		for(int i=0;i<data.size();i++){//pour chaque donnés dans la colonne
			if (value.equals(data.get(i))){
				ind.add(i);
			}
		}
		return ind;
	}
	//retourne un tableau contenant l'indice du max(0) de la colonne et la valeur(1)
	public Object[] getMaxValue(){
		int ind =0;
		Object maxObj;
			if (type.equals("Integer"))
				maxObj=Integer.MIN_VALUE;
			else if (type.equals( "Float"))
				maxObj=Float.MIN_VALUE;
			else if  (type.equals("Double"))
				maxObj=Double.MIN_VALUE;
			else
					maxObj=0;
		for (int i =0;i<data.size();i++) {
			if (type.equals("Integer")) {
				if ((Integer) data.get(i) > (Integer) maxObj) {
					ind = i;
					maxObj = data.get(i);
				}
			} else if (type.equals("Float")) {
				if ((Float) data.get(i) > (Float) maxObj) {
					ind = i;
					maxObj = data.get(i);
				}
			} else if (type.equals("Double")) {
				if ((Double) data.get(i) > (Double) maxObj) {
					ind = i;
					maxObj = data.get(i);
				}
			}
		}
		Object[] returnVal= new Object[2];
		returnVal[0]=ind;
		returnVal[1]=maxObj;
		return returnVal;
	}
	//retourne un tableau contenant l'indice du min(0) de la colonne et la valeur(1)
	public Object[] getMinValue(){
		int ind =0;
		Object minObj;
		if (type.equals("Integer"))
			minObj=Integer.MAX_VALUE;
		else if (type.equals( "Float"))
			minObj=Float.MAX_VALUE;
		else if  (type.equals("Double"))
			minObj=Double.MAX_VALUE;
		else
			minObj=0;
		for (int i =0;i<data.size();i++) {
			if (type.equals("Integer")) {
				if ((Integer) data.get(i) < (Integer) minObj) {
					ind = i;
					minObj = data.get(i);
				}
			} else if (type.equals("Float")) {
				if ((Float) data.get(i) < (Float) minObj) {
					ind = i;
					minObj = data.get(i);
				}
			} else if (type.equals("Double")) {
				if ((Double) data.get(i) < (Double) minObj) {
					ind = i;
					minObj = data.get(i);
				}
			}
		}
		Object[] returnVal= new Object[2];
		returnVal[0]=ind;
		returnVal[1]=minObj;
		return returnVal;
	}

	public Object getMoyValue(){
		if (type.equals("Integer")) {
			float sumi = 0;
			for (Object num : data) {
				sumi += (Integer) num;
			}
			return sumi / data.size();
		}else if (type.equals("Float")) {
			float sumf = 0;
			for (Object num : data) {
				sumf += (Float) num;
			}
			return sumf / data.size();
		}else if (type.equals("Double")) {
			double sumd = 0;
			for (Object num : data) {
				sumd += (Double) num;
			}
			return sumd / data.size();
		}
		return 0;
	}
	public boolean equals( Object anObject ){

		if( anObject instanceof column ) {
			column col = (column) anObject;
			return this.type.equals(col.getType()) && this.name.equals(col.getName()) && this.data.equals(col.getData());
		}
		return false;
	}

}
