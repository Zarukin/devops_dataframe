package dataframe.Exception;

public class NotSameSizeOfDataInColumn  extends Exception{
    public NotSameSizeOfDataInColumn (String s){
        super(s);
    }
}
