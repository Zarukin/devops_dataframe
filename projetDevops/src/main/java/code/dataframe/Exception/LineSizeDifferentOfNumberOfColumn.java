package dataframe.Exception;

public class LineSizeDifferentOfNumberOfColumn  extends Exception{
    public LineSizeDifferentOfNumberOfColumn (String s){
        super(s);
    }
}
