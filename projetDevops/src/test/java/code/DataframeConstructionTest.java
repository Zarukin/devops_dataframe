package code;

import org.junit.*;
import java.io.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import dataframe.*;

public class DataframeConstructionTest  {

    public DataframeConstructionTest(){ }


    @Test
    public void testConstructeur(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;
        dataframe frameExpected= null;

        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
            frameExpected = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(frameExpected,frame);

    }

    @Test(expected = Exception.class)
    public void testConstructeurException() throws Exception {
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        List<Float> listAgesFloat = new ArrayList();
        listAgesFloat.add(42.0f);
        listAgesFloat.add(24.0f);
        listAgesFloat.add(68.0f);

        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;
        frame = new dataframe("personne",listColNames,listNames,listAges,listAgesFloat);

    }
    @Test
    public void testConstructeurCSV(){


        dataframe frame = null;
        dataframe frameExpected= null;

        try {
            frameExpected = new dataframe("personne","src/main/resources/gens.csv",",");
            frame = new dataframe("personne","src/main/resources/gens.csv");

        } catch (Exception e) {
            assertEquals(true,false);
        }

        assertEquals(frameExpected,frame);

    }


    @Test
    public void testDataframeFromColumnLabel(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;

        dataframe frameExpected = null;
        List listColNamesExpected = new ArrayList();
        listColNamesExpected.add("nom");

        try {
            frameExpected = new dataframe("personne",listColNamesExpected,listNames);
            frame = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            frame = frame.dataframeFromColumnLabel("nom");
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(frameExpected,frame);
    }


    @Test
    public void testDataframeFromLineIndex(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;

        //Création du résultat attendu
        dataframe frameExpected = null;
        List listColNamesExpected = new ArrayList();
        listColNamesExpected.add("nom");

        List listNamesExpected = new ArrayList();
        listNamesExpected.add("Paul");
        listNamesExpected.add("Jacques");
        List<Integer> listAgesExpected = new ArrayList();
        listAgesExpected.add(24);
        listAgesExpected.add(68);

        List<Integer> listInds = new ArrayList();
        listInds.add(1);
        listInds.add(2);
        try {
            frameExpected = new dataframe("personne",listColNames,listNamesExpected,listAgesExpected);
            frame = new dataframe("personne",listColNames,listNames,listAges);
            frame = frame.dataframeFromLineIndex(1,2);
        } catch (Exception e) {
            e.printStackTrace();
        }


        assertEquals(frameExpected,frame);

        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
            frame = frame.dataframeFromLineIndex(listInds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(frameExpected,frame);
    }

    @Test
    public void testDataframeWhere(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;
        dataframe frameExpected = null;

        List listNamesExpected = new ArrayList();
        listNamesExpected.add("Jean");
        List<Integer> listAgesExpected = new ArrayList();
        listAgesExpected.add(42);
        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
            frameExpected = new dataframe("personne",listColNames,listNamesExpected,listAgesExpected);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            frame = frame.dataframeWhere("age",42);
        } catch (Exception e) {
            e.printStackTrace();
        }


        assertEquals(frameExpected,frame);
    }




}