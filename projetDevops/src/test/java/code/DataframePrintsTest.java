package code;

import org.junit.*;
import java.io.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import dataframe.*;

public class DataframePrintsTest  {

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final ByteArrayOutputStream err = new ByteArrayOutputStream();
    //On conserve les streams originaux pour les restaurer à la fin des tests
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    public DataframePrintsTest(){ }

    // La classe dataframe possedant des méthodes de print, on remplace les streams de base
    // pour pouvoir tester l'output avec JUnit
    @Before
    public void setStreams() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
    }

    @Test
    public void testPrint(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;


        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="------------------------personne------------------------\nindice\tnom[String]\tage[Integer]\t\n0 :\tJean\t42\t\n1 :\tPaul\t24\t\n2 :\tJacques\t68\t\n";

        try {
            frame.Print();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));

    }

    @Test
    public void testPrintEnd(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;


        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="------------------------personne------------------------\nindice\tnom[String]\tage[Integer]\t\n1 :\tPaul\t24\t\n2 :\tJacques\t68\t\n";

        try {
            frame.PrintEnd(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));


    }

    @Test
    public void testPrintBegin(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;


        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="------------------------personne------------------------\nindice\tnom[String]\tage[Integer]\t\n0 :\tJean\t42\t\n1 :\tPaul\t24\t\n";

        try {
            frame.PrintBegin(2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));


    }

    @Test
    public void testPrintMax(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;


        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
            frame.printMax();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="personne maximum colonne :\n" +
                "nom[String](Indexe)\tage[Integer](Indexe)\t\n" +
                "Donnée non comparable\t68(2)\t\n";

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }

    @Test
    public void testPrintMaxFloat(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Float> listAgesFloat = new ArrayList();
        listAgesFloat.add(42.0f);
        listAgesFloat.add(24.0f);
        listAgesFloat.add(68.0f);
        column colAge = new column("Integer","age",listAgesFloat);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;

        try {
            frame = new dataframe("personne",listColNames,listNames,listAgesFloat);
            frame.printMax();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="personne maximum colonne :\n" +
                "nom[String](Indexe)\tage[Float](Indexe)\t\n" +
                "Donnée non comparable\t68.0(2)\t\n";

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }

    @Test
    public void testPrintMaxDouble(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Double> listAgesDouble = new ArrayList();
        listAgesDouble.add(42.0);
        listAgesDouble.add(24.0);
        listAgesDouble.add(68.0);
        column colAge = new column("Integer","age",listAgesDouble);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;

        try {
            frame = new dataframe("personne",listColNames,listNames,listAgesDouble);
            frame.printMax();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="personne maximum colonne :\n" +
                "nom[String](Indexe)\tage[Double](Indexe)\t\n" +
                "Donnée non comparable\t68.0(2)\t\n";

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }

    @Test
    public void testPrintMin(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;


        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="personne minimum colonne :\n" +
                "nom[String](Indexe)\tage[Integer](Indexe)\t\n" +
                "Donnée non comparable\t24(1)\t\n";

        try {
            frame.printMin();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }


    @Test
    public void testPrintMoy(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(40);
        listAges.add(20);
        listAges.add(60);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;


        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String expected ="personne moyenne colonne :\n" +
                "nom[String]\tage[Integer]\t\n" +
                "Donnée non comparable\t40.0\n";

        try {
            frame.printMoy();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(expected.replaceAll("\n", "").replaceAll("\r", ""), out.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }

    @Test
    public void testToString(){
        // Initialisation des objets nécéssaires au test
        List listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colNom = new column("String","nom",listNames);
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        List listColNames = new ArrayList();
        listColNames.add("nom");
        listColNames.add("age");
        dataframe frame = null;
        dataframe frameExpected= null;

        try {
            frame = new dataframe("personne",listColNames,listNames,listAges);
            frameExpected = new dataframe("personne",listColNames,listNames,listAges);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(frameExpected.toString(),frame.toString());

    }

    // On restaure les streams originaux
    @After
    public void restoreInitialStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

}