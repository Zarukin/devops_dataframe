package code;

import org.junit.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import dataframe.*;

public class ColumnTest {
    public ColumnTest(){ }

    //Tests des différentes méthodes de la classe
    @Test
    public void testGetName(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column col = new column("String","nom",listNames);

        assertEquals("nom",col.getName());
    }

    @Test
    public void testGetType(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column col = new column("String","nom",listNames);

        assertEquals("String",col.getType());
    }

    @Test
    public void testGetData(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column col = new column("String","nom",listNames);

        assertEquals(listNames,col.getData());
    }

    @Test
    public void testGetDataOfInd(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column col = new column("String","nom",listNames);

        try {
            assertEquals("Jean",col.getDataOfInd(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            assertEquals("Paul",col.getDataOfInd(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            assertEquals("Jacques",col.getDataOfInd(2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddData(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column col = new column("String","nom",listNames);

        List<String> listExpected = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        listNames.add("Laura");
        column colExpected = new column("String","nom",listExpected);

        try {
            col.AddData("Laura");
        } catch (Exception e) {
            e.printStackTrace();
        }

        int i=0;
        for(Object s:colExpected.getData()){
            assertEquals(s.toString(),col.getData().get(i));
            i++;
        }

    }

    @Test
    public void testRemoveData(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column col = new column("String","nom",listNames);

        List<String> listExpected = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        column colExpected = new column("String","nom",listExpected);

        try {
            col.removeData(2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int i=0;
        for(Object s:colExpected.getData()){
            assertEquals(s.toString(),col.getData().get(i));
            i++;
        }
    }

    @Test
    public void testClone(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        column colExpected = new column("String","nom",listNames);

        column col = colExpected.clone();

        assertEquals(colExpected.getData(),col.getData());
        assertEquals(colExpected.getName(),col.getName());
        assertEquals(colExpected.getType(),col.getType());
    }

    @Test
    public void testSearch(){
        List<String> listNames = new ArrayList();
        listNames.add("Jean");
        listNames.add("Paul");
        listNames.add("Jacques");
        listNames.add("Paul");
        listNames.add("Paul");
        column col = new column("String","nom",listNames);

        List<Integer> listExpected = new LinkedList<Integer>();
        listExpected.add(1);
        listExpected.add(3);
        listExpected.add(4);

        List<Integer> list = col.search("Paul");
        assertEquals(listExpected,list);
    }
    @Test
    public void testGetMaxValue(){
        //Testing with integers
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        //Testing with floats
        List<Float> listFloat = new ArrayList();
        listFloat.add(42.0f);
        listFloat.add(24.0f);
        listFloat.add(68.0f);
        column colFloat = new column("Float","float",listFloat);
        //Testing with double
        List<Double> listDouble = new ArrayList();
        listDouble.add(42.0);
        listDouble.add(24.0);
        listDouble.add(68.0);
        column colDouble = new column("Double","double",listDouble);

        Object[] returnVal= colAge.getMaxValue();
        assertEquals(68,returnVal[1]);

        returnVal= colFloat.getMaxValue();
        assertEquals(68.0f,returnVal[1]);

        returnVal= colDouble.getMaxValue();
        assertEquals(68.0,returnVal[1]);
    }
    @Test
    public void testGetMinValue(){
        //Testing with integers
        List<Integer> listAges = new ArrayList();
        listAges.add(42);
        listAges.add(24);
        listAges.add(68);
        column colAge = new column("Integer","age",listAges);
        //Testing with floats
        List<Float> listFloat = new ArrayList();
        listFloat.add(42.0f);
        listFloat.add(24.0f);
        listFloat.add(68.0f);
        column colFloat = new column("Float","float",listFloat);
        //Testing with double
        List<Double> listDouble = new ArrayList();
        listDouble.add(42.0);
        listDouble.add(24.0);
        listDouble.add(68.0);
        column colDouble = new column("Double","double",listDouble);

        Object[] returnVal= colAge.getMinValue();
        assertEquals(24,returnVal[1]);

        returnVal= colFloat.getMinValue();
        assertEquals(24.0f,returnVal[1]);

        returnVal= colDouble.getMinValue();
        assertEquals(24.0,returnVal[1]);
    }
    @Test
    public void testGetMoyValue(){
        //Testing with integers
        List<Integer> listAges = new ArrayList();
        listAges.add(40);
        listAges.add(20);
        listAges.add(60);
        column colAge = new column("Integer","age",listAges);
        //Testing with floats
        List<Float> listFloat = new ArrayList();
        listFloat.add(40.0f);
        listFloat.add(20.0f);
        listFloat.add(60.0f);
        column colFloat = new column("Float","float",listFloat);
        //Testing with double
        List<Double> listDouble = new ArrayList();
        listDouble.add(40.0);
        listDouble.add(20.0);
        listDouble.add(60.0);
        column colDouble = new column("Double","double",listDouble);

        Object returnVal= colAge.getMoyValue();
        assertEquals(40.0f,returnVal);

        returnVal= colFloat.getMoyValue();
        assertEquals(40.0f,returnVal);

        returnVal= colDouble.getMoyValue();
        assertEquals(40.0,returnVal);
    }



}